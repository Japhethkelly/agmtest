@extends("la.layouts.app")

@section("contentheader_title", "Payments")
@section("contentheader_description", "Payments listing")
@section("section", "Payments")
@section("sub_section", "Listing")
@section("htmlheader_title", "Payments Listing")


@section("main-content")

@if (count($errors) > 0)
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif
<div>
<form method="POST" id="cakeform" action="{{url('/expresspay')}}" accept-charset="UTF-8">
        {{csrf_field()}}
				<input hidden value="5979582b121eec9e43.93707808582b1" name="merchantid" id="merchantid" >
				<input hidden value="1yj2erONXVry1YQJ78EkI-KyoDLBIzQbHdi6w24b4X-QAhJOach3IPhUkuUskSM-niPKYQsxZn3LXStH7bI" name="apikey" id="apikey" >
				<input hidden value="GHS" name="currency" id="currency" >
				
				<input hidden value="Order: 425" name="orderdesc" id="orderdesc" >
				<input hidden value="{{$order_id}}" name="orderid" id="orderid" >
	<div>
		<div style="text-align: center; display: block;margin-left: auto;margin-right: auto; width: 50%;">
  <div class="box box-success">
    <!--<div class="box-header"></div>-->
     @foreach($category as $seat)
    <div class="form-check">
      <label class="form-check-label">
        <input type="radio" class="form-check-input" name="category" id="category" value="{{$seat->price}}" onclick="calculateTotal()">{{$seat->type}} | {{$seat->description}} | GHS{{$seat->price}}
      </label>
    </div>
    @endforeach
  </div>

    <div class="form-group" style="display: block;margin-left: auto;margin-right: auto; text-align: center; width: 25%;">
    <label for="exampleSelect2">Select Number of Stalls</label>
    <select onchange="calculateTotal()" class="form-control" id="numStall" name="numStall">
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
    </select>
  </div>


    <label for="exampleSelect2">Total Cost</label>
    	<h3 name="totalprice" id="totalprice"></h3>
		<input hidden value="" name="amount" id="amount" >

  <fieldset>
				  <div class="form-group">
    <label for="exampleInputEmail1">Phone Number</label>
    <input type="text" class="form-control" id="phonenumber" name="phonenumber" aria-describedby="emailHelp" placeholder="Enter ExpressPayGh Phone Number">


    <label for="exampleInputEmail1">First Name</label>
    <input type="text" class="form-control" id="firstname" name="firstname" aria-describedby="emailHelp" placeholder="">


    <label for="exampleInputEmail1">Last Name</label>
    <input type="text" class="form-control" id="lastname" name="lastname" aria-describedby="emailHelp" placeholder="">


    <label for="exampleInputEmail1">Email</label>
    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter ExpressPayGh Email">

    <label for="exampleInputEmail1">ExpressPay Username</label>
    <input type="text" class="form-control" id="username" name="username" aria-describedby="emailHelp" placeholder="Enter ExpressPayGh Username">
  </div>

				<input hidden value="https://localhost:8000/" name="redirecturl" id="redirecturl" >
  </fieldset>

  <button type="submit" class="btn btn-primary">Proceed To ExpressPay CheckOut</button>
</div>
</form>
	</div>
	<script type="text/javascript">
/*		var numStall = document.getElementById('numStall').value;
		if (document.getElementById('category').checked) {
		var price = document.getElementById('category').value;

		document.getElementById("amount").innerHTML = price * numStall;
		}*/


// getCakeSizePrice() finds the price based on the size of the cake.
// Here, we need to take user's the selection from radio button selection
function getCakeSizePrice()
{  
    var cakeSizePrice=0;
    //Get a reference to the form id="cakeform"
    var theForm = document.forms["cakeform"];
    //Get a reference to the cake the user Chooses name=selectedCake":
    var selectedCake = theForm.elements["category"];
    //Here since there are 4 radio buttons selectedCake.length = 4
    //We loop through each radio buttons
    for(var i = 0; i < selectedCake.length; i++)
    {
        //if the radio button is checked
        if(selectedCake[i].checked)
        {
            //we set cakeSizePrice to the value of the selected radio button
            //i.e. if the user choose the 8" cake we set it to 25
            //by using the cake_prices array
            //We get the selected Items value
            //For example cake_prices["Round8".value]"
            cakeSizePrice = selectedCake[i].value;
            //If we get a match then we break out of this loop
            //No reason to continue if we get a match
            break;
        }
    }
    //We return the cakeSizePrice
    return cakeSizePrice;
}

		//This function finds the filling price based on the 
		//drop down selection
		function getFillingPrice()
		{
		    var cakeFillingPrice=0;
		    //Get a reference to the form id="cakeform"
		    var theForm = document.forms["cakeform"];
		    //Get a reference to the select id="filling"
		     var selectedFilling = theForm.elements["numStall"];
		     
		    //set cakeFilling Price equal to value user chose
		    //For example filling_prices["Lemon".value] would be equal to 5
		    cakeFillingPrice = selectedFilling.value;

		    //finally we return cakeFillingPrice
		    return cakeFillingPrice;
		}

		function calculateTotal()
		{
	    //Here we get the total price by calling our function
	    //Each function returns a number so by calling them we add the values they return together
	    var cakePrice = parseInt(getCakeSizePrice())*parseInt(getFillingPrice());
	    
	    //display the result
	    var amount = document.getElementById('amount');
	    var divobj = document.getElementById('totalprice');
	    divobj.style.display='block';
	    divobj.innerHTML = "Total Price For the Stall(s) is GHS"+cakePrice;
	    amount.value = cakePrice;
		}

	</script>
	@endsection

