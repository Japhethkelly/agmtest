@extends('la.layouts.app')

@section('htmlheader_title') Dashboard @endsection
@section('contentheader_title') Dashboard @endsection
@section('contentheader_description') Event Overview @endsection

@section('main-content')
<!-- Main content -->
<!-- <div style="position: absolute; 
left: 0;
width: 100%;
top:10%;
text-align: center;
font-size: 30px;">{{ $event->name }}</div>
<div style="position: absolute; 
left: 0;
width: 100%;
top:15%;
text-align: center;
font-size: 20px;">Event Start Date : {{ $event->eventDate }}</div> -->
<div style="width: 100%; text-align: center;">
  <h1> {{ $event->name }}</h1> <h3> {{ $event->eventDate }} </h3>  
</div>

<div class="row">
  <div class="col-xs-6">
    <div style="text-align: center; display: block;margin-left: auto;margin-right: auto;">
      <img class="img-responsive" src="{{ $img->path() }}" alt="floorplan">
    </div>
    <br>
    <br>
  </div>
  <div class="col-xs-6">

    <div style="text-align: center; display: block;margin-left: auto;margin-right: auto; width: 50%;">
      <div class="box box-success">
        <!--<div class="box-header"></div>-->
        <table class="table table-bordered" id="cases-table">
          <thead>
            <tr>
              <th>Seat Number</th>
              <th>Booked</th>
            </tr>
          </thead>

          @foreach($seats as $seat)
          @if($seat->isBooked == "Yes")
          <tr style="background-color: green;"><td>{{$seat->standnumber}}</td>
            <td>{{$seat->isBooked}}</td></tr>
            @else
            <tr><td>{{$seat->standnumber}}</td>
              <td>{{$seat->isBooked}}</td></tr>
              @endif
              @endforeach
            </table>
            <?php echo $seats->render(); ?>
          </div>
        </div>
      </div>
    </div>



    @if($reservecomplete == "No")

    <div style="text-align: center; display: block;margin-left: auto;margin-right: auto; width: 50%;">
      {{ Form::open(array('action' => 'LA\TradersController@makeReservation')) }}
      <div class="form-group">
        <label for="exampleInputEmail1">Select From The List of Available Stands and Hit Submit to Make Reservation</label>
        <br>

        <label for="exampleInputEmail1">You Have Used {{$seatingcount}} of Your {{$paycheck->numReserve}} Reservations</label>

        <div>
          <h4> You Have Reserved The Following Stands</h4>
          @foreach($seating as $availablestand)
          <h5>{{ $availablestand->standnumber }}</h5>
          @endforeach
        </div>



        <br>

        <select name="availablestands">
          @foreach($availablestands as $availablestand)
          <option value="{{ $availablestand->id}}">{{ $availablestand->standnumber }}</option>
          @endforeach
        </select>
        <br>
        <small id="emailHelp" class="form-text text-muted">Be Sure to Hit Submit to Save</small>
        <br>
        <br>

        <button type="submit" class="btn btn-primary" onclick="return confirm('Please Note - RESERVATIONS CANNOT BE UNDONE ONCE SAVED! Are you Sure, You want to make this reservation?')" >Submit</button>
      </div>


      {{Form::close()}}
    </div>


    @else
    <div style="text-align: center; display: block;margin-left: auto;margin-right: auto; width: 50%;">
      <h3>Congratulations You Have Reserved Your Spot for {{$event->name}}</h3>
      <small>Please review your details below</small>

      <h4>Stand Number : {{$seating->standnumber}} </h4>
      <h4>Date Reserved : {{$seating->updated_at}} </h4>
      <h4>Payment ID : {{$paycheck->id}}</h4>

<!-- 
  {{ Form::open(array('action' => 'LA\TradersController@changeReservation')) }}
  <div class="form-group">
    <br>
    <small id="emailHelp" class="form-text text-muted">If you to change your reservation, You can remove your current reservation and Select from the Available Stands.</small>
    <br>
    <br>

    <button type="submit" class="btn btn-primary">Remove Reservation</button>
  </div>


  {{Form::close()}} -->
</div>

@endif

@endsection

@push('styles')
<!-- Morris chart -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/morris/morris.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Date Picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ asset('la-assets/plugins/morris/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>
@endpush


@if(Session::has('msg'))

@push('scripts')
<script>
  (function($) {
    $('body').pgNotification({
      style: 'circle',
      title: 'Accra Goods Market',
      message: "{{Session::get('msg')}}",
      position: "top-right",
      timeout: 4000,
      type: "success",
      thumbnail: '<img width="40" height="40" style="display: inline-block;" src="{{ Gravatar::fallback(asset('la-assets/img/user2-160x160.jpg'))->get(Auth::user()->email, 'default') }}" data-src="assets/img/profiles/avatar.jpg" data-src-retina="assets/img/profiles/avatar2x.jpg" alt="">'
    }).show();
  })(window.jQuery);
</script>
@endpush

@else

@push('scripts')
<script>
  (function($) {
   $('body').pgNotification({
    style: 'circle',
    title: 'Accra Goods Market',
    message: "Welcome to Accra Goods Market - Admin...",
    position: "top-right",
    timeout: 5000,
    type: "success",
    thumbnail: '<img width="40" height="40" style="display: inline-block;" src="{{ Gravatar::fallback(asset('la-assets/img/user2-160x160.jpg'))->get(Auth::user()->email, 'default') }}" data-src="assets/img/profiles/avatar.jpg" data-src-retina="assets/img/profiles/avatar2x.jpg" alt="">'
  }).show();
 })(window.jQuery);
</script>

<!-- <script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
  $(function () {
    $('#cases-table').DataTable({
      processing: true,
      serverSide: true,
      ajax:  "{{ url('/seating_dt_ajax') }}",
      columns: [
      { data: 'id', name: 'id' },
      { data: 'standnumber', name: 'standnumber' },                      
      { data: 'isBooked', name: 'isBooked' }
      ],
    });
  });
</script> -->
@endpush
@endif