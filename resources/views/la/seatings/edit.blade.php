@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/seatings') }}">Seating</a> :
@endsection
@section("contentheader_description", $seating->$view_col)
@section("section", "Seatings")
@section("section_url", url(config('laraadmin.adminRoute') . '/seatings'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Seatings Edit : ".$seating->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($seating, ['route' => [config('laraadmin.adminRoute') . '.seatings.update', $seating->id ], 'method'=>'PUT', 'id' => 'seating-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'eventId')
					@la_input($module, 'traderId')
					@la_input($module, 'isBooked')
					@la_input($module, 'standnumber')
					@la_input($module, 'bookingdate')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/seatings') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#seating-edit-form").validate({
		
	});
});
</script>
@endpush
