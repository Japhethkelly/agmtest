<!DOCTYPE html>
<html lang="en">
<head>
	<title>AGM Registration</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/util.css') }}">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<!--===============================================================================================-->
</head>
<body>


	<div class="container-contact100">

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="wrap-contact100">
           <form class="contact100-form validate-form" role="form" method="POST" action="{{ url('/register') }}">
            {{ csrf_field() }}
            <span class="contact100-form-title">
               The Accra Goods Market Vendor Registration Form
           </span>

           <label class="label-input100" for="first-name">Name of Business Representive *</label>
           <div class="wrap-input100 rs1-wrap-input100 validate-input" data-validate="Type first name">
               <input id="firstname" class="input100" type="text" name="firstname" placeholder="First name" required>

               @if ($errors->has('name'))
               <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
            @endif
            <span class="focus-input100"></span>
        </div>
        <div class="wrap-input100 rs2-wrap-input100 validate-input" data-validate="Type last name" >
           <input class="input100" type="text" name="lastname" id="lastname" placeholder="Last name" required>
           <span class="focus-input100"></span>
       </div>

       <label class="label-input100" for="email">Email *</label>
       <div class="wrap-input100">
        <input id="email" type="email" name="email" class="input100" placeholder="example@example.com" required="">
        <span class="focus-input100"></span>
    </div>

    <label class="label-input100" for="email">Password *</label>
    <div class="wrap-input100">
        <input id="password" type="password" name="password" class="input100" placeholder="" required="">
        <span class="focus-input100"></span>
    </div>

    <label class="label-input100" for="email">Confirm Password *</label>
    <div class="wrap-input100">
        <input id="password_confirmation" type="password" name="password_confirmation" class="input100" placeholder="" required>
        <span class="focus-input100"></span>
    </div>

    <label class="label-input100" for="phone">Are You A New Or Existing Member? *</label>
    <div class="wrap-input100">

       <div class="btn-group" id="filterDay" data-toggle="buttons">
      
          <label class="btn btn-primary green">
             <input type="radio"  name="existingvendor"  id="existingvendor"  value="0" required>I am a new vendor
         </label>
         <label class="btn btn-primary blue">
             <input type="radio"  name="existingvendor"  id="existingvendor" value="1" > I am an Existing/Returning vendor
         </label>
     </div>
 </div>

 <label class="label-input100" for="email">Vendor/Company Name *</label>
 <div class="wrap-input100">
   <input id="vendorname" class="input100" type="text" name="vendorname" placeholder="Vendor Name" required="">
   <span class="focus-input100"></span>
</div>

<label class="label-input100" for="phone">Enter Contact Number</label>
<div class="wrap-input100">
   <input id="phone" class="input100" type="text" name="phone" placeholder="Eg. 0246890548" required="">
   <span class="focus-input100"></span>
</div>

<label class="label-input100" for="email">Social Media Handle</label>
<div class="wrap-input100">
   <input id="socialmediahandle" class="input100" type="text" name="socialmediahandle" placeholder="Eg. Facebook, Twitter">
   <span class="focus-input100"></span>
</div>

<label class="label-input100" for="email">Website</label>
<div class="wrap-input100">
    <input id="website" class="input100" type="text" name="website" placeholder="www.example.com">
    <span class="focus-input100"></span>
</div>

<label class="label-input100" >Terms of use</label>
<div class="wrap-input100">

   <div class="col-xs-9">
      <div style="border: 1px solid #e5e5e5; height: 200px; overflow: auto; padding: 10px;">

         <p>(The “Vendor”) subject to the terms and conditions outlined below (also referred to as the “Agreement”). No vendor will be allowed to enter the Trade Fair/set up unless all parties have signed the Agreement, and full payment has been received</p>

         <p>The use of Vendor space and all activities of the Vendor at THE ACCRA GOODS MARKET shall be subject to the provisions provided to the Vendor through this Agreement, and all further rules and regulations hereafter adopted for the safe and efficient conduct of the market, copies of which will be provided to Vendor before or at the Event. All such rules and regulations are made a part of the Agreement. Vendor and all of Vendor’s agents, employees, assistants or other parties related to Vendor/Exhibitor shall conform to all such rules and regulations. Vendor shall use the assigned space only for display of Vendor’s product or other items specified in this Agreement.</p>

         <p>To maintain quality, diversity and balance of Vendors, THE ACCRA GOODS MARKET reserves the right to refuse or reject any Vendor application/contract on a first-come basis and/or on past performance. THE ACCRA GOODS MARKET reserves the full right to assign to Vendors the location of all contracted spaces. THE ACCRA GOODS MARKET reserves the right to withdraw acceptance for unacceptable conduct or such other reasons as THE ACCRA GOODS MARKET in its sole discretion elects.</p>

         <p>Vendors may not sell anything not agreed upon at the market, nor sell other category products without prior consent from THE ACCRA GOODS MARKET. </p>

         <p>This Agreement shall be binding upon the parties and their respective executors, administrators, successors and assigns.</p>

         <p>Management will only refund monies paid by Vendor/Exhibitor for space reserved if a two-week notice of non-participation is served before the event.</p>


         <p>The market opens an hour (9am) before the stipulated time for vendors. Vendors must setup and be ready for patrons by 10am.</p>

         <p>NO VENDOR BANNERS ALLOWED AT THE MARKET. (Flouting of this regulation will mean no future stalls will be assigned to flouters)</p>

         <p>THE ACCRA GOODS MARKET will label vendor stalls.</p>

         <p>Vendors will be listed by name on THE ACCRA GOODS MARKET social media platforms. THE ACCRA GOODS MARKET at its discretion will decide what form vendor promotion will take.</p>


         <p>The Vendor assumes responsibility, releases and agrees to indemnify, defend and hold harmless THE ACCRA GOODS MARKET and their respective agents, employees, owners and related parties from and against any and all claims, liabilities, loss, cost or expenses arising out of the activities or in connection with the presence of the Vendor or the Vendor’s agents, employees, assistants or
         other parties related to Vendor, in connection with its presence on the grounds or the conduct of business thereon.</p>

         <p>Neither THE ACCRA GOODS MARKET, nor its agents will be held liable for any loss due to theft, vandalism, robbery, fire, accidental damage or any other loss whatsoever, for any reason whatsoever, to Vendor’s merchandise, other property or representative of the Vendor. It is the sole responsibility of the Vendor.</p>

         <p>This Agreement does not require Management to hold the event. In the unlikely event THE ACCRA GOODS MARKET is not held as proposed, or if the Event is held, but the Trade Fair is not held as proposed, Management will not be liable in any manner to the Vendor, and Management is hereby released from all claims, liabilities, costs and damages which maybe incurred by Vendor.</p>

         <p>Management will only refund monies paid by Vendor for space reserved per signed and accepted market agreement.</p>

         <p>This Agreement contains the entire agreement of the parties with respect to the subject matter of this Agreement. No modification to this Agreement shall be effective unless signed by both parties to this Agreement. This Agreement and any amendments thereto shall be governed by and construed in accordance with the laws of Ghana. In the event of a dispute under this Agreement, the parties agree to the jurisdiction of the courts of Ghana.</p>

         <p>Vendors who breach the terms and conditions of this agreement at the discretion of THE ACCRA GOODS MARKET may/may not be allowed to participate in future markets organised by THE ACCRA GOODS MARKET.</p>
     </div>
 </div>

 <div class="checkbox" style="text-align: center;">
  <label >
     <input type="checkbox" name="agree" value="agree" required="" /> Agree with the terms and conditions
 </label>
</div>
</div>

<div class="container-contact100-form-btn">
   <button type="submit" class="contact100-form-btn">
      Submit
  </button>
</div>
</form>

<div class="contact100-more flex-col-c-m" style="background-image: url('images/bg-01.jpg');">
    <div class="flex-w size1 p-b-47">
       <div class="txt1 p-r-25">
          <span class="lnr lnr-map-marker"></span>
      </div>

      <div class="flex-col size2">
          <span class="txt1 p-b-20">
             Address
         </span>

         <span class="txt2">
             The Accra Goods Market Pop Up Store, 14 Jungle Road, A&C Shopping Mall Accra, East Legon Accra.
         </span>
     </div>
 </div>

 <div class="dis-flex size1 p-b-47">
   <div class="txt1 p-r-25">
      <span class="lnr lnr-phone-handset"></span>
  </div>

  <div class="flex-col size2">
      <span class="txt1 p-b-20">
         Lets Talk
     </span>

     <span class="txt3">
         +233 55 598 7331
     </span>
 </div>
</div>

<div class="dis-flex size1 p-b-47">
   <div class="txt1 p-r-25">
      <span class="lnr lnr-envelope"></span>
  </div>

  <div class="flex-col size2">
      <span class="txt1 p-b-20">
         General Support
     </span>

     <span class="txt3">
         hello@theaccragoodsmarket.com
     </span>
 </div>
</div>
</div>
</div>
</div>



<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/bootstrap/js/popper.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="vendor/select2/select2.min.js"></script>
<script>
  $(".selection-2").select2({
     minimumResultsForSearch: 20,
     dropdownParent: $('#dropDownSelect1')
 });
</script>
<!--===============================================================================================-->
<script src="vendor/daterangepicker/moment.min.js"></script>
<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="js/main.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
</body>
</html>
