@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/traders') }}">Trader</a> :
@endsection
@section("contentheader_description", $trader->$view_col)
@section("section", "Traders")
@section("section_url", url(config('laraadmin.adminRoute') . '/traders'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Traders Edit : ".$trader->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($trader, ['route' => [config('laraadmin.adminRoute') . '.traders.update', $trader->id ], 'method'=>'PUT', 'id' => 'trader-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'company')
					@la_input($module, 'paid')
					@la_input($module, 'mNumber')
					@la_input($module, 'email')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/traders') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#trader-edit-form").validate({
		
	});
});
</script>
@endpush
