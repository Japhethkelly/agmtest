<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/traderRegister', 'LA\TradersController@register');
Route::post('/paybefore', 'LA\DashboardController@paybefore');
Route::post('/traderRegister', 'LA\TradersController@addtrader');

Route::post('/makeReservation', 'LA\TradersController@makeReservation');
Route::post('/changeReservation', 'LA\TradersController@changeReservation');

Route::get('/traderdashboard', 'LA\DashboardController@traderIndex');
Route::get('/seating_dt_ajax', 'LA\DashboardController@dtajax');
Route::get('/reservationcomplete', 'LA\DashboardController@reservationcomplete');

Route::get('/payment', 'LA\PaymentsController@makepaymentpage');
Route::post('/makepayment', 'LA\PaymentsController@makepayment');

Route::post('/expresspay', 'LA\PaymentsController@expresspay');

Route::put('/afterpay/{orderid}/{token}', 'LA\PaymentsController@afterpay');

Route::auth();
Route::get('/user/activation/{token}', 'Auth\AuthController@activateUser')->name('user.activate');


Route::get('/socialtest', function()
{
	return Share::page('http://jorenvanhocht.be')->facebook();
});

/* ================== Homepage + Admin Routes ================== */

require __DIR__.'/admin_routes.php';
