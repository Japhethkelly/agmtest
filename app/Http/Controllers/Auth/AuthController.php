<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Models\Employee;
use App\Role;
use Validator;
use Eloquent;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\ActivationService;
use App\Models\Trader;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = "/admin";
    protected $activationService;

        /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
        public function __construct(ActivationService $activationService)
        {
            $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
            $this->activationService = $activationService;
        }

        public function redirectPath()
        {
            if (\Auth::user()->type == 'Trader') {
                return "/traderdashboard";
        // or return route('routename');
            }

            return "/admin";
        // or return route('routename');
        }

        public function register(Request $request)
        {
            $validator = $this->validator($request->all());

            if ($validator->fails()) {
                $this->throwValidationException(
                    $request, $validator
                );
            }

            $user = $this->create($request->all());

            $this->activationService->sendActivationMail($user);

            return redirect('/login')->with('status', 'We sent you an activation code. Check your email.');
        }

        public function showRegistrationForm()
        {
            $roleCount = Role::count();
            if($roleCount != 0) {
               $userCount = User::count();
               if($userCount == 0) {
                return view('auth.register');
            } else {
                return redirect('login');
            }
        } else {
           return view('errors.error', [
            'title' => 'Migration not completed',
            'message' => 'Please run command <code>php artisan db:seed</code> to generate required table data.',
        ]);
       }
   }

   public function showLoginForm()
   {
      $roleCount = Role::count();
      if($roleCount != 0) {
       $userCount = User::count();
       if($userCount == 0) {
        return redirect('register');
    } else {
        return view('auth.login');
    }
} else {
   return view('errors.error', [
    'title' => 'Migration not completed',
    'message' => 'Please run command <code>php artisan db:seed</code> to generate required table data.',
]);
}
}

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'existingvendor' => 'required',
        ]);
    }

    public function addTrader(Request $request)
    {
        $trader = new Trader;


        $trader->name = $request->firstname." ".$request->lastname;
        $trader->company = $request->vendorname;
        $trader->mNumber = $request->phone;
        $trader->email = $request->email;
        $trader->website = $request->website;
        $trader->socialmediahandle = $request->socialhandle;
        $trader->isNew = $request->existingvendor;

        $trader->save();
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        // TODO: This is Not Standard. Need to find alternative
        Eloquent::unguard();
        
        $trader = Trader::create([
            'name' => $data['firstname']." ".$data['lastname'],
            'company' => $data['vendorname'],
            'mNumber' => $data['phone'],
            'email' => $data['email'],
            'website' => $data['website'],
            'socialmediahandle' => $data['socialmediahandle'],
            'isNew' => $data['existingvendor'],
        ]);

        /*$employee = Employee::create([
            'name' => $data['name'],
            'designation' => "Super Admin",
            'mobile' => "8888888888",
            'mobile2' => "",
            'email' => $data['email'],
            'gender' => 'Male',
            'dept' => "1",
            'city' => "Pune",
            'address' => "Karve nagar, Pune 411030",
            'about' => "About user / biography",
            'date_birth' => date("Y-m-d"),
            'date_hire' => date("Y-m-d"),
            'date_left' => date("Y-m-d"),
            'salary_cur' => 0,
        ]);*/
        
        $user = User::create([
            'name' => $data['firstname']." ".$data['lastname'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'context_id' => $trader->id,
            'type' => "Trader",
        ]);
        $role = Role::where('name', 'TRADER')->first();
        $user->attachRole($role);

        return $user;
    }

    public function authenticated(Request $request, $user)
    {
        if (!$user->activated) {
            $this->activationService->sendActivationMail($user);
            auth()->logout();
            return back()->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
        }
        return redirect()->intended($this->redirectPath());
    }

    public function activateUser($token)
    {
        if ($user = $this->activationService->activateUser($token)) {
            auth()->login($user);
            return redirect($this->redirectPath());
        }
        abort(404);
    }
}
