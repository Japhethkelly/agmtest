<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Trader;
use App\Models\Seating;
use App\Models\Payment;
use App\Models\Event;
use App\User;
use Illuminate\Support\Facades\Hash;

class TradersController extends Controller
{
	public $show_action = true;
	public $view_col = 'email';
	public $listing_cols = ['id', 'name', 'company', 'paid', 'mNumber', 'email'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Traders', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Traders', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Traders.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Traders');
		
		if(Module::hasAccess($module->id)) {
			return View('la.traders.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for creating a new trader.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created trader in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Traders", "create")) {

			$rules = Module::validateRules("Traders", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Traders", $request);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.traders.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified trader.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Traders", "view")) {
			
			$trader = Trader::find($id);
			if(isset($trader->id)) {
				$module = Module::get('Traders');
				$module->row = $trader;
				
				return view('la.traders.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('trader', $trader);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("trader"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified trader.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Traders", "edit")) {			
			$trader = Trader::find($id);
			if(isset($trader->id)) {	
				$module = Module::get('Traders');
				
				$module->row = $trader;
				
				return view('la.traders.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('trader', $trader);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("trader"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified trader in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Traders", "edit")) {
			
			$rules = Module::validateRules("Traders", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Traders", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.traders.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified trader from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Traders", "delete")) {
			Trader::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.traders.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}


	public function register()
	{
		return view("la.traders.register");
	}
	

	public function addTrader(Request $request)
	{
		$trader = new Trader;

		$trader->name = $request->firstname." ".$request->lastname;
		$trader->company = $request->vendorname;
		$trader->mNumber = $request->phone;
		$trader->email = $request->email;
		$trader->website = $request->website;
		$trader->socialmediahandle = $request->socialhandle;
		$trader->isNew = $request->isNew;

		$trader->save();

		$user = new User;

		$user->name = $request->firstname." ".$request->lastname;
		$user->email = $request->email;
		$user->password = Hash::make($request->password);
		$user->type = "Trader";

		$user->save();

		return redirect("/traderdashboard");
	}

	public function makeReservation(Request $request){
		$trader = Trader::where("email", Auth::user()->email)->first();
		$event = Event::orderBy('created_at', 'desc')->first();
		
		$seating = Seating::find($request->availablestands);
		$seating->isBooked = "Yes";
		$seating->traderId = $trader->id;

		$seating->save();


		$payment = Payment::where('eventId', $event->id)->where('traderId', $trader->id)->first();
		$curSeat = Seating::orderBy('updated_at', 'desc')->first();
		//$payment->isReserve = "Yes";
		$payment->seatingId = $curSeat->id;
		$payment->save();
		return redirect('/traderdashboard');
		
	}

	public function changeReservation(Request $request){
		$trader = Trader::where("email", Auth::user()->email)->first();
		$event = Event::orderBy('created_at', 'desc')->first();
		$payment = Payment::where('eventId', $event->id)->where('traderId', $trader->id)->first();
		
		$seating = Seating::find($payment->seatingId);
		$seating->isBooked = "No";
		$seating->traderId = null;

		$payment->seatingId = null;
		$payment->isReserve ="No";

		$seating->save();
		$payment->save();

		return redirect('/traderdashboard');		
	}
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('traders')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Traders');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/traders/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Traders", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/traders/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Traders", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.traders.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}
