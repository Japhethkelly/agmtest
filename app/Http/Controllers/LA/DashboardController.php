<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Upload;
use App\Models\Event;
use App\Models\Seating;
use App\Models\Payment;
use App\Models\Trader;
use Datatables;
use Share;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $traderId = Trader::where('email', auth()->user()->email)->first();
        $event = Event::orderBy('created_at', 'desc')->first();
        $payCount = Payment::count();
        $traderCount = Trader::count();
        $bouncerate=($payCount/$traderCount)*100;
        $bouncerate= number_format((float)$bouncerate, 2, '.', '');
        return view('la.dashboard')->with("payCount",$payCount)->with("traderCount",$traderCount)->with('event',$event)->with('bouncerate',$bouncerate);
    }

    public function reservationcomplete(){

        $event = Event::orderBy('created_at', 'desc')->first();
        $seats = Seating::where('eventId', $event->id)->paginate(15);
        $traderId = Trader::where('email', auth()->user()->email)->first();
        $img = Upload::find($event->floorplan);
        $paycheck = Payment::where('eventId', $event->id)->where('traderId', $traderId->id)->where('ispaycomplete', 1)->first();

        $seating = Seating::where('eventId', $event->id)->where('traderId', $traderId->id)->get();
        $seatArray="";
        foreach($seating as $se){
            $seatArray = $seatArray.','. $se->standnumber;
        }
        $share= Share::load("www.theaccragoodsmarket.com", 'Hey Guys. Be Sure to Check Out Our Stand, Number '.$seatArray.'  at The Accra Goods Market on'.$event->eventDate)->facebook();
        $sharetwitter= Share::load("www.theaccragoodsmarket.com", 'Hey Guys. Be Sure to Check Out Our Stand, Number '.$seatArray.'  at The Accra Goods Market on'.$event->eventDate)->twitter();

    //   return view('la.reservationcomplete')->with('share',$share)->with('seating',$seating);

        return view('la.reservationcomplete')->with('img', $img)->with('event',$event)->with('seating',$seating)->with('paycheck',$paycheck)->with('share',$share)->with('sharetwitter',$sharetwitter)->with('seating',$seating);
    }

    public function traderIndex()
    {

        $event = Event::orderBy('created_at', 'desc')->first();
        $seats = Seating::where('eventId', $event->id)->paginate(15);
        $traderId = Trader::where('email', auth()->user()->email)->first();
        $availablestands = Seating::where('eventId', $event->id)->where('isBooked','No')->get();
        $img = Upload::find($event->floorplan);

        DB::enableQueryLog();
        $paycheck = Payment::where('eventId', $event->id)->where('traderId', $traderId->id)->where('ispaycomplete', 1)->first();

        $seatingcount = Seating::where('eventId', $event->id)->where('traderId', $traderId->id)->count();


        if ($paycheck== null){
            return view('paybefore')->with('traderid', $traderId)->with('event', $event)->with('paycheck', $paycheck);
        }
        elseif($seatingcount ==$paycheck->numReserve){
            
            return redirect('reservationcomplete');           
        }else{
            $seating = Seating::where('eventId', $event->id)->where('traderId', $traderId->id)->get();
            $reservecomplete = "No";
            return view('la.traderdashboard')->with('img', $img)->with('event',$event)->with('seats',$seats)->with('availablestands',$availablestands)->with('paycheck',$paycheck)->with('seating',$seating)->with('seatingcount',$seatingcount)->with('reservecomplete',$reservecomplete);}
        }



        public function dtajax()
        {
        //DB::enableQueryLog();
            $event = Event::orderBy('created_at', 'desc')->first();
            $values = Seating::where('eventId',$event->id)->get();
            $out = Datatables::of($values)->make(true);

        //    dd(DB::getQueryLog());

            return $out;
        /*$data = $out->getData();

        $fields_popup = ModuleFields::getModuleFields('Traders');
        
        for($i=0; $i < count($data->data); $i++) {
            for ($j=0; $j < count($this->listing_cols); $j++) { 
                $col = $this->listing_cols[$j];
                if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
                    $data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
                }
                if($col == $this->view_col) {
                    $data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/traders/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
                }
                // else if($col == "author") {
                //    $data->data[$i][$j];
                // }
            }
        }
        $out->setData($data);
        return $out;*/
    }
}

